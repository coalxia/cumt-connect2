# CumtConnect2

#### 介绍
用于便捷连接校园多节点网络的桌面端应用


#### 安装教程

1.  到Gitee发布页下载可执行程序的压缩包到本地
2.  解压
3.  运行“CumtConnect2.exe”

#### 使用说明

1.  点击“个人校园网账号”储存你的校园网信息
2.  连接到校内网络（CUMT_Stu/CUMT_Tec等）
3.  点击“登录@CUMT_Stu”等待弹窗提示结果

#### 参与贡献

-   Star 本仓库
-   对程序存在的任何问题都可以提Issue

#### 说明
1. 本程序仅适用于Windows系统，推荐使用Windows10及以上版本。Win7/8用户参见：[下载运行依赖框架](https://dotnet.microsoft.com/en-us/download/dotnet-framework/net472 )。
2. 连接网络后，服务器将会帮助你保持最新。
